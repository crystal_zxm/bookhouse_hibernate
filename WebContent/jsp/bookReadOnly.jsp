<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>图书信息</title>

<script type="text/javascript">
	function loadPublisher() {
		document.orm.submit();
	}
</script>

</head>
<body>
                编号:<input type="text" disabled="disabled" value="${book.serialnumber}"/>
		<br>
		 书名:<input type="text"  disabled="disabled" value="${book.name}"/>
		<br>
		作者:<input type="text" disabled="disabled" value="${book.author}"/>
		<br>
		 出版社:<input type="text" disabled="disabled" value="${book.press.name}"/>
		<br>
		 数量:<input type="text" disabled="disabled" value="${book.amount}"/>
		<br>
</body>
</html>
