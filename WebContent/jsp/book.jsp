<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>图书信息</title>

<script type="text/javascript">
	function load() {
		document.bookForm.action = "${basePath}book/Book_getBookEdit";
		document.bookForm.submit();
	}
</script>

</head>
<body>

	<s:iterator value="fieldErrors">
		<font color=red> <s:property value="value[0]" />
		</font>
		<br>
	</s:iterator>

	<s:form name="bookForm" action="/book/Book_saveBook" method="post">
		<input type="hidden" name="book.id" value="${book.id}">
		编号:<input type="text" name="book.serialnumber"
			value="${book.serialnumber}" />
		<br>
		 书名:<input type="text" name="book.name" value="${book.name}" />
		<br>
		作者:<input type="text" name="book.author" value="${book.author}" />
		<br>
		 数量:<input type="text" name="book.amount" value="${book.amount}" />
		<br>

		<!-- 	
		分类:<br>&nbsp&nbsp分类1:
		<c:if test="${not empty categoryList}">
			<select name="book.cate1" onchange="javascript:load()">
				<option value="-1">全部</option>
				<c:forEach var="category" varStatus="status" items="${categoryList}">
					<c:choose>
						<c:when test="${category.id == book.cate1 }">
							<option value="${category.id}" selected="selected">${category.name}</option>
						</c:when>
						<c:otherwise>
							<option value="${category.id}">${category.name }</option>
						</c:otherwise>
					</c:choose>
				</c:forEach>
			</select>
			<br>
		</c:if>



		<c:if test="${not empty categoryList1}">
		&nbsp&nbsp分类2:
			<select name="book.cate2" onchange="javascript:load()">
				<option value="-1">全部</option>
				<c:forEach var="category" varStatus="status"
					items="${categoryList1}">
					<c:choose>
						<c:when test="${category.id == book.cate2 }">
							<option value="${category.id}" selected="selected">${category.name}</option>
						</c:when>
						<c:otherwise>
							<option value="${category.id}">${category.name }</option>
						</c:otherwise>
					</c:choose>
				</c:forEach>
			</select>
		</c:if>


		<br>
		<c:if test="${not empty categoryList2}">
		&nbsp&nbsp分类3:
			<select name="book.cate3" onchange="javascript:load()">
				<option value="-1">全部</option>
				<c:forEach var="category" varStatus="status"
					items="${categoryList2}">
					<c:choose>
						<c:when test="${category.id == book.cate3 }">
							<option value="${category.id}" selected="selected">${category.name}</option>
						</c:when>
						<c:otherwise>
							<option value="${category.id}">${category.name }</option>
						</c:otherwise>
					</c:choose>
				</c:forEach>
			</select>
			<br>
		</c:if>


		 
		<c:if test="${not empty categoryList3}">
			&nbsp&nbsp分类4:<select name="book.cate4" onchange="javascript:load()">
				<option value="-1">全部</option>
				<c:forEach var="category" varStatus="status"
					items="${categoryList3}">
					<c:choose>
						<c:when test="${category.id == book.cate4 }">
							<option value="${category.id}" selected="selected">${category.name}</option>
						</c:when>
						<c:otherwise>
							<option value="${category.id}">${category.name }</option>
						</c:otherwise>
					</c:choose>
				</c:forEach>
			</select>
			<br>
		</c:if>



		<c:if test="${not empty categoryList4}">
			&nbsp&nbsp分类5:<select name="book.cate5" onchange="javascript:load()">
				<option value="-1">全部</option>
				<c:forEach var="category" varStatus="status"
					items="${categoryList4}">
					<c:choose>
						<c:when test="${category.id == book.cate5 }">
							<option value="${category.id}" selected="selected">${category.name}</option>
						</c:when>
						<c:otherwise>
							<option value="${category.id}">${category.name }</option>
						</c:otherwise>
					</c:choose>
				</c:forEach>
			</select>
			<br>
		</c:if>
		
-->
			
		
		出版社:
		<c:if test="${not empty presses}">
			<select name="book.press.id" onchange="javascript:load()">
				<option value="-1">全部</option>
				<c:forEach var="press" items="${presses}">
					<c:choose>
						<c:when test="${press.id == book.press.id }">
							<option value="${press.id}" selected="selected">${press.name}</option>
						</c:when>
						<c:otherwise>
							<option value="${press.id}">${press.name }</option>
						</c:otherwise>
					</c:choose>
				</c:forEach>
			</select>
		</c:if>
		<br>
		<s:submit value="保存" />
	</s:form>

	<a href="${basePath}/book/Book_getBookList" target="mainContent">图书列表</a>
</body>
</html>
