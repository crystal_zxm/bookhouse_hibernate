<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE HTML>
<html>
<head>
<title>图书列表</title>
<script type="text/javascript">
	function editBook() {
		var radios = document.getElementsByName("choosen");
		var bookId = "";
		for (var i = 0; i < radios.length; i++) {
			if (radios[i].checked == true) {
				bookId = radios[i].value;
				break;
			}
		}
		if (bookId == "") {
			alert("请选择需要操作的出版社");
		} else {
			document.getElementById("bookId").value = bookId;
			document.operationForm.action = "${basePath}/book/Book_getBookEdit.action";
			document.operationForm.submit();
		}
	}

	function deleteBook() {
		var radios = document.getElementsByName("choosen");
		var bookId = "";
		for (var i = 0; i < radios.length; i++) {
			if (radios[i].checked == true) {
				bookId = radios[i].value;
				break;
			}
		}
		if (bookId == "") {
			alert("请选择需要操作的课程");
		} else if (confirm('确认删除吗？')) {
			document.getElementById("bookId").value = bookId;
			document.operationForm.action = "${basePath}/book/Book_deleteBook.action";
			document.operationForm.submit();
		}
	}

	function addBook() {
		window.location.href = '${basePath}/book/Book_getBookEdit.action';
	}

	function listBook() {
		window.location.href = '${basePath}/book/Book_getBookList.action';
	}
</script>
</head>
<body>
	<h2>图书信息列表</h2>
	<form name="operationForm" action="" method="post" class="anniu">
		<input type="hidden" id="bookId" name="bookId" value=""> <input
			type="button" onclick="javascript:addBook()" value="新增"> <input
			type="button" onclick="javascript:editBook()" value="修改"> <input
			type="button" onclick="javascript:deleteBook()" value="删除"> <input
			type="button" onclick="javascript:listBook()" value="所有图书">
	</form>
	<s:form action="/book/Book_getBookSearch" method="post">
		<s:textfield name="keyword" label="查询图书:" />
		<s:submit value="查询" />
	</s:form>
	<hr>
	<table>
		<thead>
			<tr>
				<th></th>
				<th>编号</th>
				<th>书名</th>
				<th>作者</th>
				<th>出版社</th>
				<th>数量</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach var="list" items="${list}">
				<tr>
					<td><input type="radio" name="choosen" value="${list.id}"></td>
					<td>${list.serialnumber}</td>
					<td>${list.name}</td>
					<td>${list.author}</td>
					<td>${list.press.name}</td>
					<td>${list.amount}</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
</body>
</html>