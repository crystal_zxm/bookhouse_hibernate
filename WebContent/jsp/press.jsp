<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>出版社信息</title>
<link rel="stylesheet" href="${basePath}/css/normalize.css" />
<link rel="stylesheet" href="${basePath}/css/default.css">
<link rel="stylesheet" href="${basePath}/css/styles.css">
<style>
article .panel-lite {
	margin: 0;
	max-width: none;
}

.floating-btn {
	display: none;
}

article.htmleaf-container {
	height: 700px;
}
</style>
</head>
<body>
	<article class="htmleaf-container">
	<div class="panel-lite">
		<h1>编辑出版社</h1>
		<s:iterator value="fieldErrors">
			<font color=red> <s:property value="value[0]" />
			</font>
			<br>
		</s:iterator>
		<s:form name="categoryForm"
			action="/press/Press_savePress" method="post">
			 <input type="hidden" name="press.id" value="${press.id}">

			<!--<div class="form-group">
				<input type="text" name="press.name" value="${press.name}"
					required="required" class="form-control" /> <label
					class="form-label">出版社名称 </label>
			</div>
			<div class="form-group">
				<input type="text" name="press.code" value="${press.code}"
					required="required" class="form-control" /> <label
					class="form-label">输入码 </label>
			</div>
			<div class="form-group">
				<input type="text" name="press.isbn" value="${press.isbn}"
					required="required" class="form-control" /> <label
					class="form-label">isbn </label>
			</div>
			<div class="form-group">
				<input type="text" name="press.place" value="${press.place}"
					required="required" class="form-control" /> <label
					class="form-label">出版地 </label>
			</div>
			<button class="floating-btn">
				<i class="icon-arrow"></i>
			</button> -->
			出版社名称:<input type="text" name="press.name"
				value="${press.name}" />
			<br>
		 输入码:<input type="text" name="press.code"
				value="${press.code}" />
			<br>
		isbn:<input type="text" name="press.isbn"
				value="${press.isbn}" />
			<br>
		 出版地:<input type="text" name="press.place"
				value="${press.place}" />
			<br>
			<s:submit value="保存" />
		</s:form>
	</div>
	</article>
</body>
</html>
