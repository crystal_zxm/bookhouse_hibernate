<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE HTML>
<html>
<head>
<title>读者列表</title>

</head>

<script type="text/javascript">
	function editReader() {
		var radios = document.getElementsByName("choosen");
		var readerId = "";
		for (var i = 0; i < radios.length; i++) {
			if (radios[i].checked == true) {
				readerId = radios[i].value;
				break;
			}
		}
		if (readerId == "") {
			alert("请选择需要操作的出版社");
		} else {
			document.getElementById("readerId").value = readerId;
			document.operationForm.action = "${basePath}/reader/Reader_getReaderEdit.action";
			document.operationForm.submit();
		}
	}

	function deleteReader() {
		var radios = document.getElementsByName("choosen");
		var readerId = "";
		for (var i = 0; i < radios.length; i++) {
			if (radios[i].checked == true) {
				readerId = radios[i].value;
				break;
			}
		}
		if (readerId == "") {
			alert("请选择需要操作的课程");
		} else if (confirm('确认删除吗？')) {
			document.getElementById("readerId").value = readerId;
			document.operationForm.action = "${basePath}/reader/Reader_deleteReader.action";
			document.operationForm.submit();
		}
	}

	function addReader() {
		window.location.href = '${basePath}/reader/Reader_getReaderEdit.action';
	}

	function listReader() {
		window.location.href = '${basePath}/reader/Reader_getReaderList.action';
	}
</script>
<body>
	<h2>读者信息列表</h2>
	<form name="operationForm" action="" method="post" class="anniu">
		<input type="hidden" id="readerId" name="readerId" value=""> <input
			type="button" onclick="javascript:addReader()" value="新增"> <input
			type="button" onclick="javascript:editReader()" value="修改"> <input
			type="button" onclick="javascript:deleteReader()" value="删除"> <input
			type="button" onclick="javascript:listReader()" value="所有读者">
	</form>

	<s:form action="/reader/Reader_getReaderSearch" method="post">
		<s:textfield name="keyword" label="查询读者:" />
		<s:submit value="查询" />
	</s:form>
	<hr>
	<table>
		<thead>
			<tr>
				<th></th>
				<th>编号</th>
				<th>姓名</th>
				<th>类别</th>
				<th>电话</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach var="list" items="${list}">
				<tr>
					<td><input type="radio" name="choosen" value="${list.id}"></td>
					<td>${list.id}</td>
					<td>${list.name}</td>
					<td>${list.readertype.name}</td>
					<td>${list.phone}</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
</body>
</html>