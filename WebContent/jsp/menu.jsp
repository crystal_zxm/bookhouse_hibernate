<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title></title>

<link rel="stylesheet" href="../css/bootstrap.min.css">
<link rel="stylesheet" href="../css/index.css">
<script src="js/jquery-1.11.1.js"></script>
<script src="js/bootstrap.min.js"></script>
<script type="text/javascript">
	$(function() {
		$('#collapse4').collapse({
			toggle : false
		});
		$('#collapse3').collapse({
			toggle : false
		});

		$('#collapse2').collapse({
			toggle : false
		});
		$('#collapse1').collapse({
			toggle : false
		});
	});
</script>
</head>
<body>
	<%-- <%@include file="header.jsp"%> --%>
	<div>
		<div class="row">
			<div class="span3">
				<div class="well">
					<!--侧面的菜单外围-->
					<c:forEach var="menu" items="${list}" varStatus="status">
						<div class="panel-group" id="accordion">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<a data-toggle="collapse" data-parent="#accordion"
											href="#collapse${status.index+1}">
											${menu.name}&nbsp&nbsp&nbsp<<< </a>
									</h4>
								</div>
								<div id="collapse${status.index+1}"
									class="panel-collapse collapse in">
									<div class="panel-body">
										<c:forEach var="menu1" items="${menu.list}">
											<c:choose>
												<c:when test="${menu1.display =='bookEdit' }">
													<a href="${basePath}Menu_getSite.action?name=${menu1.display}"
														target="">${menu1.name}</a>
												</c:when>
												<c:otherwise>
													<a href="${basePath}Menu_getSite.action?name=${menu1.display}"
														target="mainContent">${menu1.name}</a>
													<br>
												</c:otherwise>
											</c:choose>
										</c:forEach>
									</div>
								</div>
							</div>
						</div>
					</c:forEach>
				</div>
			</div>
			<div class="span9">
				<div class="well">
					<iframe id="mainContent" name="mainContent" width="100%"
					height="100%" frameborder="0" scrolling="auto" src="../default.html">
					
					</iframe>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
