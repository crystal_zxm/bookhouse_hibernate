<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>读者类别信息</title>
</head>
<body>
	<h1>编辑读者类别信息</h1>
	<s:iterator value="fieldErrors">
		<font color=red> <s:property value="value[0]" />
		</font>
		<br>
	</s:iterator>

	<s:form name="typeForm"
		action="/readertype/Readertype_insertReadertype" method="post">
		<input type="hidden" name="readertype.id" value="${readertype.id}">
		类别名称:<input type="text" name="readertype.name"
			value="${readertype.name}" />
		<br>
		可借书数:<input type="text" name="readertype.bookMax"
			value="${readertype.bookMax}" />
		<br>
		可借天数:<input type="text" name="readertype.dayMax"
			value="${readertype.dayMax}" />
		<br>
		 可续借天数:<input type="text" name="readertype.continueDay"
			value="${readertype.continueDay}" />
		<br>
		<s:submit value="保存" />
	</s:form>

</body>
</html>
