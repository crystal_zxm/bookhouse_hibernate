<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>读者类型列表</title>
</head>
<script type="text/javascript">
	function editType() {
		var radios = document.getElementsByName("choosen");
		var typeId = "";
		for (var i = 0; i < radios.length; i++) {
			if (radios[i].checked == true) {
				typeId = radios[i].value;
				break;
			}
		}
		if (typeId == "") {
			alert("请选择需要操作的出版社");
		} else {
			document.getElementById("typeId").value = typeId;
			document.operationForm.action = "${basePath}/readertype/Readertype_getReadertypeEdit.action";
			document.operationForm.submit();
		}
	}

	function deleteType() {
		var radios = document.getElementsByName("choosen");
		var typeId = "";
		for (var i = 0; i < radios.length; i++) {
			if (radios[i].checked == true) {
				typeId = radios[i].value;
				break;
			}
		}
		if (typeId == "") {
			alert("请选择需要操作的课程");
		} else if (confirm('确认删除吗？')) {
			document.getElementById("typeId").value = typeId;
			document.operationForm.action = "${basePath}/readertype/Readertype_deleteReadertype.action";
			document.operationForm.submit();
		}
	}

	function addType() {
		window.location.href = '${basePath}/jsp/readertype.jsp';
	}
</script>
<body>
	<h2>出版社列表</h2>
	<form name="operationForm" action="" method="post" class="anniu">
		<input type="hidden" id="typeId" name="typeId" value=""> <input
			type="button" onclick="javascript:addType()" value="新增"> <input
			type="button" onclick="javascript:editType()" value="修改"> <input
			type="button" onclick="javascript:deleteType()" value="删除">
	</form>

	<hr>
	<table>
		<thead>
			<tr>
				<th></th>
				<th>类别名称</th>
				<th>可借书数</th>
				<th>可借天数</th>
				<th>可续借天数</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach var="list" items="${list}">
				<tr>
					<td><input type="radio" name="choosen" value="${list.id}"></td>
					<td>${list.name}</td>
					<td>${list.bookMax}</td>
					<td>${list.dayMax}</td>
					<td>${list.continueDay}</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>

</body>
</html>
