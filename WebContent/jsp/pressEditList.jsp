<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>出版社列表</title>
</head>
<script type="text/javascript">
	function editPress() {
		var radios = document.getElementsByName("choosen");
		var pressId = "";
		for (var i = 0; i < radios.length; i++) {
			if (radios[i].checked == true) {
				pressId = radios[i].value;
				break;
			}
		}
		if (pressId == "") {
			alert("请选择需要操作的出版社");
		} else {
			document.getElementById("pressId").value = pressId;
			document.operationForm.action = "${basePath}/press/Press_getPressEdit.action";
			document.operationForm.submit();
		}
	}

	function deletePress() {
		var radios = document.getElementsByName("choosen");
		var pressId = "";
		for (var i = 0; i < radios.length; i++) {
			if (radios[i].checked == true) {
				pressId = radios[i].value;
				break;
			}
		}
		if (pressId == "") {
			alert("请选择需要操作的课程");
		} else if (confirm('确认删除吗？')) {
			document.getElementById("pressId").value = pressId;
			document.operationForm.action = "${basePath}/press/Press_deletePress.action";
			document.operationForm.submit();
		}
	}

	function addPress() {
		window.location.href = '${basePath}/jsp/press.jsp';
	}

	function listPress() {
		window.location.href = '${basePath}/press/Press_getPressList.action';
	}
</script>
<body>
	<h2>出版社列表</h2>
	<form name="operationForm" action="" method="post" class="anniu">
		<input type="hidden" id="pressId" name="pressId" value="">
		<input type="button" onclick="javascript:addPress()" value="新增">
		<input type="button" onclick="javascript:editPress()" value="修改">
		<input type="button" onclick="javascript:deletePress()" value="删除">
		<input type="button" onclick="javascript:listPress()"
			value="所有出版社">
	</form>

	<s:form action="/press/Press_getPressSearch" method="post">
		<s:textfield name="keyword" label="查询出版社" />
		<s:submit value="查询" />
	</s:form>
	<hr>
	<table>
		<thead>
			<tr>
				<th></th>
				<th>出版单位名称</th>
				<th>输入码</th>
				<th>ISBN</th>
				<th>出版地</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach var="list" items="${list}">
				<tr>
					<td><input type="radio" name="choosen" value="${list.id}"></td>
					<td>${list.name}</td>
					<td>${list.code}</td>
					<td>${list.isbn}</td>
					<td>${list.place}</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>

</body>
</html>
