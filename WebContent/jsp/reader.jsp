<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>用户信息</title>
</head>
<body>

	<s:form name="readerForm" action="reader/Reader_addReader"
		method="post">
		<input type="hidden" name="reader.id" value="${reader.id}">
		读者姓名:
		<input type="text" name="reader.name" value="${reader.name}" />
		<br>
		读者电话:
		<input type="text" name="reader.phone" value="${reader.phone}" />
		<br>
		读者类型:
		<select name="reader.readertype.id">
			<option value="-1">全部</option>
			<c:forEach var="type" items="${types}">
				<c:choose>
					<c:when test="${type.id == reader.readertype.id }">
						<option value="${type.id}" selected="selected">${type.name}</option>
					</c:when>
					<c:otherwise>
						<option value="${type.id}">${type.name }</option>
					</c:otherwise>
				</c:choose>
			</c:forEach>
		</select>
		<s:submit value="保存" />
	</s:form>
</body>
</html>
