package com.bookhouse.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

public class ApplicationFilter implements Filter {

	@Override
	public void destroy() {
		// TODO Auto-generated method stub

	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		ServletContext application = request.getServletContext();
		if (application.getAttribute("basePath") == null) {
			String path = application.getContextPath();
			// ��ñ���Ŀ�ĵ�ַ(����: http://localhost:8080/MyApp/)��ֵ��basePath����
			String basePath = request.getScheme() + "://"
					+ request.getServerName() + ":" + request.getServerPort()
					+ path + "/";
			// �� "��Ŀ·��basePath" ����pageContext�У����Ժ���EL���ʽ������
			application.setAttribute("basePath", basePath);
		}

		chain.doFilter(request, response);
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
		// TODO Auto-generated method stub

	}

}
