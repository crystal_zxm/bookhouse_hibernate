package com.bookhouse.service;

import java.util.List;

import com.bookhouse.dao.DAOException;
import com.bookhouse.dao.DAOFactory;
import com.bookhouse.dao.ReaderDAO;
import com.bookhouse.domain.Reader;

public class ReaderService {

	private ReaderDAO dao = DAOFactory.getReaderDAO();

	public void save(Reader reader) {
		try {
			if (reader.getId() != null && reader.getId() > 0)
				dao.updateReader(reader);
			else
				dao.insert(reader);
		} catch (DAOException e) {
			e.printStackTrace();
		}
	}

	public List<Reader> getReader() {
		List<Reader> readers = null;
		try {
			readers = dao.getReader();
		} catch (DAOException e) {
			e.printStackTrace();
		}
		return readers;
	}

	public Reader getReader(int id) {
		Reader reader = new Reader();
		try {
			reader = dao.getReader(id);
		} catch (DAOException e) {
			e.printStackTrace();
		}
		return reader;
	}

	public void deleteReader(int id) {
		try {
			dao.deleteReader(id);
		} catch (DAOException e) {
			e.printStackTrace();
		}
	}

	public void deleteAllReader() {
		try {
			dao.deleteAllReader();
		} catch (DAOException e) {
			e.printStackTrace();
		}
	}
}
