package com.bookhouse.service;

import java.util.List;

import com.bookhouse.dao.DAOException;
import com.bookhouse.dao.DAOFactory;
import com.bookhouse.dao.ReadertypeDAO;
import com.bookhouse.domain.Readertype;


public class ReadertypeService {
	private ReadertypeDAO dao = DAOFactory.getReadertypeDAO();

	public void addReadertype(Readertype type) {
		try {
			if (type.getId() !=null && type.getId() > 0)
				dao.updateReadertype(type);
			else
				dao.insert(type);
		} catch (DAOException e) {
			e.printStackTrace();
		}
	}

	public List<Readertype> getReadertype() {
		List<Readertype> types = null;
		try {
			types = dao.getReadertype();
		} catch (DAOException e) {
			e.printStackTrace();
		}
		return types;
	}

	public Readertype getReadertype(int id) {
		Readertype type = null;
		try {
			type = dao.getReadertype(id);
		} catch (DAOException e) {
			e.printStackTrace();
		}
		return type;
	}

	public void deleteReadertype(int id) {
		try {
			dao.deleReadertype(id);
		} catch (DAOException e) {
			e.printStackTrace();
		}
	}

}
