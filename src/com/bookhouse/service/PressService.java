package com.bookhouse.service;

import java.util.List;

import com.bookhouse.dao.DAOException;
import com.bookhouse.dao.DAOFactory;
import com.bookhouse.dao.PressDAO;
import com.bookhouse.domain.Press;

public class PressService {

	private PressDAO dao = DAOFactory.getPressDAO();

	public List<Press> getPress() {
		List<Press> presses = null;
		try {
			presses = dao.getPress();
		} catch (DAOException e) {
			e.printStackTrace();
		}
		return presses;
	}

	public List<Press> getPress(String keyword) {
		List<Press> presses = null;
		try {
			presses = dao.getPress(keyword);
		} catch (DAOException e) {
			e.printStackTrace();
		}
		return presses;
	}

	public Press getPress(int id) {
		Press press = new Press();
		try {
			press = dao.getPress(id);
		} catch (DAOException e) {
			e.printStackTrace();
		}
		return press;
	}

	public void savePress(Press press) {
		try {
			if (press.getId() != null && press.getId() > 0)
				dao.updatePress(press);
			else
				dao.insertPress(press);
		} catch (DAOException e) {
			e.printStackTrace();
		}
	}

	public void deletePress(int id) {
		try {
			dao.deletePress(id);
		} catch (DAOException e) {
			e.printStackTrace();
		}
	}

	public void deleteAllPress() {
		try {
			dao.deleteAllPress();
		} catch (DAOException e) {
			e.printStackTrace();
		}
	}
}
