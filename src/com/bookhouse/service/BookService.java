package com.bookhouse.service;

import java.util.List;

import com.bookhouse.dao.BookDAO;
import com.bookhouse.dao.DAOException;
import com.bookhouse.dao.DAOFactory;
import com.bookhouse.domain.Book;

public class BookService {
	private BookDAO dao = DAOFactory.getBookDAO();

	public void save(Book book) {
		try {
			if (book.getId() != null && book.getId() > 0)
				dao.updateBook(book);
			else
				dao.insert(book);
		} catch (DAOException e) {
			e.printStackTrace();
		}
	}

	public List<Book> getBooks() {
		List<Book> books = null;
		try {
			books = dao.getBooks();
		} catch (DAOException e) {
			e.printStackTrace();
		}
		return books;
	}

	public List<Book> getBook(String keyword) {
		List<Book> books = null;
		try {
			books = dao.getBook(keyword);
		} catch (DAOException e) {
			e.printStackTrace();
		}
		return books;
	}

	public List<Book> getBookLevel1(int cate1) {
		List<Book> books = null;
		try {
			books = dao.getBookLevel1(cate1);
		} catch (DAOException e) {
			e.printStackTrace();
		}
		return books;
	}

	public List<Book> getBookLevel2(int cate2) {
		List<Book> books = null;
		try {
			books = dao.getBookLevel2(cate2);
		} catch (DAOException e) {
			e.printStackTrace();
		}
		return books;
	}

	public List<Book> getBookLevel3(int cate3) {
		List<Book> books = null;
		try {
			books = dao.getBookLevel3(cate3);
		} catch (DAOException e) {
			e.printStackTrace();
		}
		return books;
	}

	public Book getBook(int id) {
		Book book = new Book();
		try {
			book = dao.getBook(id);
		} catch (DAOException e) {
			e.printStackTrace();
		}
		return book;
	}

	public void deleteBook(int id) {
		try {
			dao.deleteBook(id);
		} catch (DAOException e) {
			e.printStackTrace();
		}
	}

	public void deleteAllBook() {
		boolean isSuccess = false;
		try {
			dao.deleteAllBook();
		} catch (DAOException e) {
			e.printStackTrace();
		}
	}
}
