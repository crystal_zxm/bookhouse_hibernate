package com.bookhouse.service;

import java.util.List;

import com.bookhouse.dao.DAOException;
import com.bookhouse.dao.DAOFactory;
import com.bookhouse.dao.MenuDAO;
import com.bookhouse.domain.Menu;

public class MenuService {
	private MenuDAO dao = DAOFactory.getMenuDAO();

	public List<Menu> getMenu() {
		List<Menu> list = null;
		try {
			list = dao.getMenu();
		} catch (DAOException e) {
			e.printStackTrace();
		}
		return list;
	}
}
