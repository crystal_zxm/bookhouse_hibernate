package com.bookhouse.action;

import java.util.List;

import org.apache.struts2.interceptor.validation.SkipValidation;

import com.bookhouse.domain.Reader;
import com.bookhouse.domain.Readertype;
import com.bookhouse.service.ReaderService;
import com.bookhouse.service.ReadertypeService;

public class ReaderAction {

	private Reader reader = new Reader();
	private String rePassword;
	private String name;
	private String password;
	private List<Reader> list;
	private List<Readertype> types;
	private String readerId;

	private ReaderService readerService = new ReaderService();
	private ReadertypeService typeService = new ReadertypeService();

	public Reader getReader() {
		return reader;
	}

	public void setReader(Reader reader) {
		this.reader = reader;
	}

	public String getRePassword() {
		return rePassword;
	}

	public void setRePassword(String rePassword) {
		this.rePassword = rePassword;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public List<Reader> getList() {
		return list;
	}

	public void setList(List<Reader> list) {
		this.list = list;
	}

	public List<Readertype> getTypes() {
		return types;
	}

	public void setTypes(List<Readertype> types) {
		this.types = types;
	}

	public String getReaderId() {
		return readerId;
	}

	public void setReaderId(String readerId) {
		this.readerId = readerId;
	}

	public String addReader() throws Exception {
		readerService.save(reader);
		return "addReader";
	}

	@SkipValidation
	public String getReaderList() throws Exception {
		list = readerService.getReader();
		return "getReaderList";
	}

	@SkipValidation
	public String getReaderEdit() throws Exception {
		types = typeService.getReadertype();
		if (readerId != null && !readerId.isEmpty()) {
//			reader.setId(Integer.parseInt(readerId));
			reader = readerService.getReader(Integer.parseInt(readerId));
		}
		return "getReaderEdit";
	}

	@SkipValidation
	public String getReaderBorrow() throws Exception {
		reader.setId(Integer.parseInt(readerId));
		reader = readerService.getReader(reader.getId());
		return "getReaderBorrow";
	}

	@SkipValidation
	public String deleteAllReader() throws Exception {
		readerService.deleteAllReader();
		return "deleteAllReader";
	}
}
