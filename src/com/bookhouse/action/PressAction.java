package com.bookhouse.action;

import java.util.List;

import org.apache.struts2.interceptor.validation.SkipValidation;

import com.bookhouse.domain.Press;
import com.bookhouse.service.PressService;
import com.opensymphony.xwork2.ActionSupport;

public class PressAction extends ActionSupport {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private List<Press> list;
	private Press press;
	private String pressId;
	private String keyword;
	private PressService pressService = new PressService();

	public List<Press> getList() {
		return list;
	}

	public void setList(List<Press> list) {
		this.list = list;
	}

	public Press getPress() {
		return press;
	}

	public void setPress(Press press) {
		this.press = press;
	}

	public String getPressId() {
		return pressId;
	}

	public void setPressId(String pressId) {
		this.pressId = pressId;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	@SkipValidation
	public String getPressList() throws Exception {
		list = pressService.getPress();
		return "getPressList";
	}

	@SkipValidation
	public String getPressSearch() throws Exception {
		list = pressService.getPress(keyword);
		return "getPressSearch";
	}

	@SkipValidation
	public String getPressEdit() throws Exception {
		int i = Integer.parseInt(pressId);
		press = pressService.getPress(i);
		return "getPressEdit";
	}

	@SkipValidation
	public String deletePress() throws Exception {
		pressService.deletePress(Integer.parseInt(pressId));
		return "deletePress";
	}

	public String savePress() throws Exception {
		pressService.savePress(press);
		return "savePress";
	}

	@SkipValidation
	public String form() throws Exception {
		return "form";
	}

	@SkipValidation
	public String deleteAllPress() throws Exception {
		pressService.deleteAllPress();
		return "deleteAllPress";
	}
}
