package com.bookhouse.action;

import java.util.List;

import com.bookhouse.domain.Menu;
import com.bookhouse.service.MenuService;
import com.opensymphony.xwork2.ActionSupport;

public class MenuAction extends ActionSupport {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private List<Menu> list;
	// private Manager manager;
	private String id;
	private String name;
	private String password;
	private MenuService service = new MenuService();
	// private ManagerService service1 = new ManagerService();

	public List<Menu> getList() {
		return list;
	}

	public void setList(List<Menu> list) {
		this.list = list;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getMenu() throws Exception {
		// manager = service1.checkLogin(name, password);
		list = service.getMenu();
		return "getMenu";
	}

	// public String getSite() throws Exception {
	// manager = service1.checkLogin(name, password);
	// if (name.equals("publisherEdit"))
	// return "publisherEdit";
	//
	// if (name.equals("bookEdit"))
	// return "bookList";
	//
	// if (name.equals("readerType"))
	// return "readerType";
	//
	// if (name.equals("editReader"))
	// return "editReader";
	//
	// if (name.equals("borrowBook"))
	// return "borrowBook";
	//
	// if (name.equals("returnBook"))
	// return "getReturnRecord";
	//
	// if (name.equals("allRecord"))
	// return "allRecord";
	//
	// if (name.equals("overdueRecord"))
	// return "overdueRecord";
	//
	// if (name.equals("addAdmin"))
	// return "addAdmin";
	//
	// if (name.equals("countRecord"))
	// return "countRecord";
	//
	// if (name.equals("editManager"))
	// return "editManager";
	//
	// if (name.equals("tool"))
	// return "tool";
	// else
	// return "allRecord";
	// }
}
