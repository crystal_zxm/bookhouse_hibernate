package com.bookhouse.action;

import java.util.List;

import org.apache.struts2.interceptor.validation.SkipValidation;

import com.bookhouse.domain.Readertype;
import com.bookhouse.service.ReadertypeService;

public class ReadertypeAction {
	private List<Readertype> list;
	private Readertype readertype;
	private String typeId;
	private ReadertypeService service = new ReadertypeService();

	public List<Readertype> getList() {
		return list;
	}

	public void setList(List<Readertype> list) {
		this.list = list;
	}

	public Readertype getReadertype() {
		return readertype;
	}

	public void setReadertype(Readertype readertype) {
		this.readertype = readertype;
	}

	public String getTypeId() {
		return typeId;
	}

	public void setTypeId(String typeId) {
		this.typeId = typeId;
	}

	@SkipValidation
	public String insertReadertype() throws Exception {
		service.addReadertype(readertype);
		return "insertReadertype";
	}

	@SkipValidation
	public String getReadertypeList() throws Exception {
		list = service.getReadertype();
		return "getReadertypeList";
	}

	public String getReadertypeEdit() throws Exception {
		if (typeId != null && !typeId.isEmpty()) {
//			readertype.setId(Integer.parseInt(typeId));
			readertype = service.getReadertype(Integer.parseInt(typeId));
		}
		return "getReadertypeEdit";
	}

	public String deleteReadertype() throws Exception {
		service.deleteReadertype(Integer.parseInt(typeId));
		return "deleteReadertype";
	}

	public String updateReadertype() throws Exception {

		return "updateReadertype";
	}
}
