package com.bookhouse.action;

import java.io.Reader;
import java.util.List;

import org.apache.struts2.interceptor.validation.SkipValidation;

import com.bookhouse.domain.Book;
import com.bookhouse.domain.Press;
import com.bookhouse.service.BookService;
import com.bookhouse.service.PressService;
import com.opensymphony.xwork2.ActionSupport;

public class BookAction extends ActionSupport {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Book book = new Book();
	private Press press = new Press();
	private List<Book> list;
	private List<Press> presses;
	// private List<Bookcate> categoryList;
	// private List<Bookcate> categoryList1;
	// private List<Bookcate> categoryList2;
	// private List<Bookcate> categoryList3;
	// private List<Bookcate> categoryList4;
	private String bookId;
	// private Reader reader = new Reader();
	// private String readerId;
	private String pressId;
	// private String cate1;
	// private String cate2;
	// private String cate3;
	// private String categoryId1;
	private String keyword;

	private BookService bookService = new BookService();
	// private ReaderService readerService = new ReaderService();
	private PressService pressService = new PressService();
	// private CategoryService categoryService = new CategoryService();

	public Book getBook() {
		return book;
	}

	public void setBook(Book book) {
		this.book = book;
	}

	public List<Book> getList() {
		return list;
	}

	public void setList(List<Book> list) {
		this.list = list;
	}

	public String getBookId() {
		return bookId;
	}

	public void setBookId(String bookId) {
		this.bookId = bookId;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public Press getPress() {
		return press;
	}

	public void setPress(Press press) {
		this.press = press;
	}

	public List<Press> getPresses() {
		return presses;
	}

	public void setPresses(List<Press> presses) {
		this.presses = presses;
	}

	public String getPressId() {
		return pressId;
	}

	public void setPressId(String pressId) {
		this.pressId = pressId;
	}

	public String saveBook() throws Exception {
		bookService.save(book);
		return "saveBook";
	}

	@SkipValidation
	public String getBookList() throws Exception {
		list = bookService.getBooks();
		return "getBookList";
	}

	@SkipValidation
	public String getBookSearch() throws Exception {
		list = bookService.getBook(keyword);
		return "getBookSearch";
	}

	@SkipValidation
	public String getBookEdit() throws Exception {
		// ActionContext ctx = ActionContext.getContext();
		// Map<String, Object> session = ctx.getSession();
		// if (categoryId1 != null && !categoryId1.isEmpty()) {
		// session.put("categoryId1", Integer.parseInt(categoryId1));
		// }
		if (bookId != null && !bookId.isEmpty()) {
			book.setId(Integer.parseInt(bookId));
			book = bookService.getBook(book.getId());
		}
		presses = pressService.getPress();
		// categoryList = categoryService.getCategoryLsitByParent(0);
		// categoryList1 =
		// categoryService.getCategoryLsitByParent(book.getCate1());
		// categoryList2 =
		// categoryService.getCategoryLsitByParent(book.getCate2());

		return "getBookEdit";

	}

	@SkipValidation
	public String getBookBorrow() throws Exception {
		book.setId(Integer.parseInt(bookId));
		book = bookService.getBook(book.getId());
		// reader.setId(Integer.parseInt(readerId));
		// reader = readerService.getReader(reader.getId());
		return "getBookBorrow";
	}

	@SkipValidation
	public String deleteBook() throws Exception {
		book.setId(Integer.parseInt(bookId));
		bookService.deleteBook(book.getId());
		System.out.print(book);
		return "deleteBook";
	}

	// @SkipValidation
	// public String getBookLevel1() throws Exception {
	// list = bookService.getBookLevel1(Integer.parseInt(cate1));
	// return "getBookLevel1";
	// }
	//
	// @SkipValidation
	// public String getBookLevel2() throws Exception {
	// list = bookService.getBookLevel2(Integer.parseInt(cate2));
	// return "getBookLevel2";
	// }
	//
	// @SkipValidation
	// public String getBookLevel3() throws Exception {
	// list = bookService.getBookLevel3(Integer.parseInt(cate3));
	// return "getBookLevel3";
	// }

	@SkipValidation
	public String deleteAllBook() throws Exception {
		bookService.deleteAllBook();
		return "deleteAllBook";
	}
}
