package com.bookhouse.domain;

import java.util.List;

import javax.persistence.*;

@Entity
@Table(name = "menu_inf")
public class Menu {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	private Integer parentId;
	private String name;
	private String display;
	private Integer level;
	private List<Menu> list;

	public Menu() {
	}

	public Menu(Integer parentId, String name, String display, Integer level, List<Menu> list) {
		this.parentId = parentId;
		this.name = name;
		this.display = display;
		this.level = level;
		this.list = list;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getParentId() {
		return parentId;
	}

	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDisplay() {
		return display;
	}

	public void setDisplay(String display) {
		this.display = display;
	}

	public Integer getLevel() {
		return level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}

	public List<Menu> getList() {
		return list;
	}

	public void setList(List<Menu> list) {
		this.list = list;
	}

}
