package com.bookhouse.domain;

import javax.persistence.*;

@Entity
@Table(name = "readertype_inf")
public class Readertype {

	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	private String name;
	private Integer bookMax;
	private Integer dayMax;
	private Integer continueDay;

	public Readertype() {
	}

	public Readertype(String name, Integer bookMax, Integer dayMax, Integer continueDay) {
		this.name = name;
		this.bookMax = bookMax;
		this.dayMax = dayMax;
		this.continueDay = continueDay;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getBookMax() {
		return bookMax;
	}

	public void setBookMax(Integer bookMax) {
		this.bookMax = bookMax;
	}

	public Integer getDayMax() {
		return dayMax;
	}

	public void setDayMax(Integer dayMax) {
		this.dayMax = dayMax;
	}

	public Integer getContinueDay() {
		return continueDay;
	}

	public void setContinueDay(Integer continueDay) {
		this.continueDay = continueDay;
	}

}
