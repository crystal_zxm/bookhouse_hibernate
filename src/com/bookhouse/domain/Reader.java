package com.bookhouse.domain;

import java.sql.Timestamp;

import javax.persistence.*;

@Entity
@Table(name = "reader_inf")
public class Reader {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	private String name;
	private String phone;
	private Timestamp createTime;
	private Integer isAbled;

	@ManyToOne(targetEntity = Readertype.class)
	@JoinColumn(name = "typeId", nullable = false)
	private Readertype readertype;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Timestamp getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}

	public Integer getIsAbled() {
		return isAbled;
	}

	public void setIsAbled(Integer isAbled) {
		this.isAbled = isAbled;
	}

	public Readertype getReadertype() {
		return readertype;
	}

	public void setReadertype(Readertype readertype) {
		this.readertype = readertype;
	}

}
