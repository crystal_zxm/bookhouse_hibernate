package com.bookhouse.dao;

import java.util.List;

import com.bookhouse.domain.Book;

public interface BookDAO extends DAO {
	void insert(Book book) throws DAOException;

	List<Book> getBooks() throws DAOException;

	List<Book> getBook(String keyword) throws DAOException;

	Book getBook(int id) throws DAOException;

	void updateBook(Book book) throws DAOException;

	void updateBookAmount(Book book) throws DAOException;

	void deleteBook(int id) throws DAOException;

	void deleteAllBook() throws DAOException;

	List<Book> getBookLevel1(int cate1) throws DAOException;

	List<Book> getBookLevel2(int cate2) throws DAOException;

	List<Book> getBookLevel3(int cate3) throws DAOException;
}
