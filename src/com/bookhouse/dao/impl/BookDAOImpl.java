package com.bookhouse.dao.impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.bookhouse.dao.BaseDAO;
import com.bookhouse.dao.BookDAO;
import com.bookhouse.dao.DAOException;
import com.bookhouse.domain.Book;
import com.bookhouse.util.HibernateSessionFactory;

public class BookDAOImpl extends BaseDAO implements BookDAO {

	@Override
	public void insert(Book book) throws DAOException {
		Transaction tx = null;
		try {
			Session session = HibernateSessionFactory.getSession();
			tx = session.beginTransaction();
			session.save(book);
			tx.commit();
		} catch (Exception ex) {
			throw new DAOException("Error adding book. " + ex.getMessage());
		} finally {
			if (tx != null)
				tx = null;
			HibernateSessionFactory.closeSession();
		}

	}

	@Override
	public List<Book> getBooks() throws DAOException {
		List<Book> list = null;
		try {
			Session session = HibernateSessionFactory.getSession();
			String hql = "from Book";
			list = session.createQuery(hql).list();
		} catch (Exception ex) {
			throw new DAOException("Error getting book. " + ex.getMessage());
		} finally {
			HibernateSessionFactory.closeSession();
		}
		return list;
	}

	@Override
	public List<Book> getBook(String keyword) throws DAOException {
		Transaction tx = null;
		List<Book> list = null;
		String hql = "from Book where name like :keyword";
		try {
			Session session = HibernateSessionFactory.getSession();
			tx = session.beginTransaction();
			list = session.createQuery(hql).setString("keyword", keyword).list();
			tx.commit();
		} catch (Exception ex) {
			throw new DAOException("Error getting book. " + ex.getMessage());
		} finally {
			if (tx != null)
				tx = null;
			HibernateSessionFactory.closeSession();
		}
		return list;
	}

	@Override
	public Book getBook(int id) throws DAOException {
		Book book = null;
		try {
			Session session = HibernateSessionFactory.getSession();
			book = (Book) session.get(Book.class, id);
		} catch (Exception ex) {
			throw new DAOException("Error getting book. " + ex.getMessage());
		} finally {
			HibernateSessionFactory.closeSession();
		}
		return book;
	}

	@Override
	public void updateBook(Book book) throws DAOException {
		Transaction tx = null;
		try {
			Session session = HibernateSessionFactory.getSession();
			tx = session.beginTransaction();
			session.update(book);
			tx.commit();
		} catch (Exception ex) {
			throw new DAOException("Error updating book. " + ex.getMessage());
		} finally {
			if (tx != null)
				tx = null;
			HibernateSessionFactory.closeSession();
		}
	}

	@Override
	public void updateBookAmount(Book book) throws DAOException {
		Transaction tx = null;
		try {
			Session session = HibernateSessionFactory.getSession();
			tx = session.beginTransaction();
			session.update(book);
			tx.commit();
		} catch (Exception ex) {
			throw new DAOException("Error updating book. " + ex.getMessage());
		} finally {
			if (tx != null)
				tx = null;
			HibernateSessionFactory.closeSession();
		}
	}

	@Override
	public void deleteBook(int id) throws DAOException {
		Transaction tx = null;
		try {
			Session session = HibernateSessionFactory.getSession();
			tx = session.beginTransaction();
			Book book = (Book) session.get(Book.class, id);
			session.delete(book);
			tx.commit();
		} catch (Exception ex) {
			throw new DAOException("Error deleting book. " + ex.getMessage());
		} finally {
			if (tx != null)
				tx = null;
			HibernateSessionFactory.closeSession();
		}
	}

	@Override
	public void deleteAllBook() throws DAOException {
		// TODO Auto-generated method stub
	}

	@Override
	public List<Book> getBookLevel1(int cate1) throws DAOException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Book> getBookLevel2(int cate2) throws DAOException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Book> getBookLevel3(int cate3) throws DAOException {
		// TODO Auto-generated method stub
		return null;
	}

}
