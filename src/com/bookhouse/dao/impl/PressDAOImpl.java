package com.bookhouse.dao.impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.bookhouse.dao.BaseDAO;
import com.bookhouse.dao.DAOException;
import com.bookhouse.dao.PressDAO;
import com.bookhouse.domain.Press;
import com.bookhouse.util.HibernateSessionFactory;

public class PressDAOImpl extends BaseDAO implements PressDAO {

	@Override
	public List<Press> getPress() throws DAOException {
		List<Press> list = null;
		try {
			Session session = HibernateSessionFactory.getSession();
			String hql = "from Press";
			list = session.createQuery(hql).list();
		} catch (Exception ex) {
			throw new DAOException("Error getting book. " + ex.getMessage());
		} finally {
			HibernateSessionFactory.closeSession();
		}
		return list;
	}

	@Override
	public List<Press> getPress(String keyword) throws DAOException {
		Transaction tx = null;
		List<Press> list = null;
		String hql = "from Press p where name like :keyword";
		try {
			Session session = HibernateSessionFactory.getSession();
			tx = session.beginTransaction();
			list = session.createQuery(hql).setString("keyword", keyword).list();
			tx.commit();
		} catch (Exception ex) {
			throw new DAOException("Error getting book. " + ex.getMessage());
		} finally {
			if (tx != null)
				tx = null;
			HibernateSessionFactory.closeSession();
		}
		return list;
	}

	@Override
	public Press getPress(int id) throws DAOException {
		Press press = null;
		try {
			Session session = HibernateSessionFactory.getSession();
			press = (Press) session.get(Press.class, id);
		} catch (Exception ex) {
			throw new DAOException("Error getting book. " + ex.getMessage());
		} finally {
			HibernateSessionFactory.closeSession();
		}
		return press;
	}

	@Override
	public void insertPress(Press press) throws DAOException {
		Transaction tx = null;
		try {
			Session session = HibernateSessionFactory.getSession();
			tx = session.beginTransaction();
			session.save(press);
			tx.commit();
		} catch (Exception ex) {
			throw new DAOException("Error adding book. " + ex.getMessage());
		} finally {
			if (tx != null)
				tx = null;
			HibernateSessionFactory.closeSession();
		}
	}

	@Override
	public void updatePress(Press press) throws DAOException {
		Transaction tx = null;
		try {
			Session session = HibernateSessionFactory.getSession();
			tx = session.beginTransaction();
			session.update(press);
			tx.commit();
		} catch (Exception ex) {
			throw new DAOException("Error updating book. " + ex.getMessage());
		} finally {
			if (tx != null)
				tx = null;
			HibernateSessionFactory.closeSession();
		}
	}

	@Override
	public void deletePress(int id) throws DAOException {
		Transaction tx = null;
		try {
			Session session = HibernateSessionFactory.getSession();
			tx = session.beginTransaction();
			Press press = (Press) session.get(Press.class, id);
			session.delete(press);
			tx.commit();
		} catch (Exception ex) {
			throw new DAOException("Error deleting book. " + ex.getMessage());
		} finally {
			if (tx != null)
				tx = null;
			HibernateSessionFactory.closeSession();
		}
	}

	@Override
	public void deleteAllPress() throws DAOException {
		Transaction tx = null;
		try {
			Session session = HibernateSessionFactory.getSession();
			tx = session.beginTransaction();
			tx.commit();
		} catch (Exception ex) {
			throw new DAOException("Error deleting book. " + ex.getMessage());
		} finally {
			if (tx != null)
				tx = null;
			HibernateSessionFactory.closeSession();
		}
	}

}
