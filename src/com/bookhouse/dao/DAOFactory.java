package com.bookhouse.dao;

import com.bookhouse.dao.impl.BookDAOImpl;
import com.bookhouse.dao.impl.MenuDAOImpl;
import com.bookhouse.dao.impl.PressDAOImpl;
import com.bookhouse.dao.impl.ReaderDAOImpl;
import com.bookhouse.dao.impl.ReadertypeDAOImpl;

public class DAOFactory {

	public static PressDAO getPressDAO() {
		return new PressDAOImpl();
	}

	public static ReaderDAO getReaderDAO() {
		return new ReaderDAOImpl();
	}

	public static BookDAO getBookDAO() {
		return new BookDAOImpl();
	}

	// public static RecordDAO getRecordDAO() {
	// return new RecordDAOImpl();
	// }
	//
	//
	//
	// public static CategoryDAO getCategotyDAO() {
	// return new CategoryDAOImpl();
	// }
	//
	public static MenuDAO getMenuDAO() {
		return new MenuDAOImpl();
	}

	// public static ManagerDAO getManagerDAO() {
	// return new ManagerDAOImpl();
	// }

	public static ReadertypeDAO getReadertypeDAO() {
		return new ReadertypeDAOImpl();
	}
}
