package com.bookhouse.dao;

import java.util.List;

import com.bookhouse.domain.Press;


public interface PressDAO extends DAO{
	
	List<Press> getPress() throws DAOException;

	List<Press> getPress(String keyword) throws DAOException;

	Press getPress(int id) throws DAOException;

	void insertPress(Press press) throws DAOException;

	void updatePress(Press press) throws DAOException;

	void deletePress(int id) throws DAOException;

	void deleteAllPress() throws DAOException;

}
