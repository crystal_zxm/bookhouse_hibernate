package com.bookhouse.dao;

import java.util.List;

import com.bookhouse.domain.Reader;

public interface ReaderDAO extends DAO {
	void insert(Reader reader) throws DAOException;

	List<Reader> getReader() throws DAOException;

	Reader getReader(int id) throws DAOException;

	void updateReader(Reader reader) throws DAOException;

	void deleteReader(int id) throws DAOException;

	void deleteAllReader() throws DAOException;
}
