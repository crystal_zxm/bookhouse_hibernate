package com.bookhouse.dao;

import java.util.List;

import com.bookhouse.domain.Menu;

public interface MenuDAO extends DAO {
	List<Menu> getMenu() throws DAOException;
}
