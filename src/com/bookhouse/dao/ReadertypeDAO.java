package com.bookhouse.dao;

import java.util.List;

import com.bookhouse.domain.Readertype;

public interface ReadertypeDAO extends DAO {

	void insert(Readertype readertype) throws DAOException;

	List<Readertype> getReadertype() throws DAOException;

	Readertype getReadertype(int id) throws DAOException;

	void updateReadertype(Readertype readertype) throws DAOException;

	void deleReadertype(int id) throws DAOException;
}
